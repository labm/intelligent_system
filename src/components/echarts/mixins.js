export default {
  methods: {
    handleResize (myCharts) {
      if (myCharts) {
        window.onresize = () => {
          this.myCharts && this.myCharts.resize();
        }
      }
    }
  },
  destroyed () {
    window.resize = null;
  }
}
