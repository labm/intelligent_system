import api from './index.js'

// axios
import request from '@/utils/request'
//媒体力量对比分析
export function Analysis(data){
  return request({
    url: api.Analysis,
    method: 'get',
    data
  })
}
//声量变化趋势数据
export function Trend(data){
  return request({
    url: api.Trend,
    method: 'get',
    data
  })
}
//受众分布(用户分布统计)
export function Statistics(data){
  return request({
    url: api.Statistics,
    method: 'get',
    data
  })
}
//平台数据分布
export function Distribution(data){
  return request({
    url: api.Distribution,
    method: 'get',
    data
  })
}