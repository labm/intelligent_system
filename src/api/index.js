const api = {
  //首页
  Analysis:'/media/analysis', //媒体力量对比分析
  Trend:'/volume/trend', //声量变化趋势数据
  Statistics:'/user/statistics', //受众分布(用户分布统计)
  Distribution:'/platform/distribution', //平台数据分布
}

export default api