import Vue from 'vue'
import ElementUI from 'element-ui';
import 'element-ui/lib/theme-chalk/index.css';
import echarts from 'echarts';
import echartWordcloud from 'echarts-wordcloud';
import App from './power.vue'

Vue.prototype.$echarts = echarts

Vue.use(ElementUI);
Vue.use(echartWordcloud);

new Vue({
  // store,
  render: h => h(App)
}).$mount('#app')
