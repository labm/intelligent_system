import axios from 'axios'

// 根据环境不同引入不同api地址
import baseApi from '@/config'
// create an axios instance
const service = axios.create({
  baseURL: baseApi.baseApi, // url = base api url + request url
  withCredentials: true, // send cookies when cross-domain requests
  timeout: 10000 // request timeout
})

// request拦截器 request interceptor
service.interceptors.request.use(
  config => {
    return config
  },
  error => {
    // do something with request error
    console.log(error) // for debug
    return Promise.reject(error)
  }
)
// respone拦截器
service.interceptors.response.use(
  response => {
    const res = response
    return Promise.resolve(res)
  },
  error => {
    console.log('err' + error) // for debug
    return Promise.reject(error)
  }
)


export default service

// this.mediaDetail = data.mediaDetail
//       let mediaDetail = data.mediaDetail
//       for (let i = 0; i < mediaDetail.length; i++) {
//         let jData = mediaDetail[i].data
//         this.powerSource.push({
//           'powername': mediaDetail[i].mediaType,
//           [jData[0].stance]: baifenbi[i + 1][0],
//           [jData[1].stance]: baifenbi[i + 1][1],
//           [jData[2].stance]: baifenbi[i + 1][2],
//         });
//         this.mediaDetail[i].proportion = data.source[i+1]
//         for(let j=0;j<jData.length;j++){
//           this.mediaDetail[i].data[j].percent = baifenbi[i + 1][j]
//         }
//       }