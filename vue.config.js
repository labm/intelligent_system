let path = require('path')
let glob = require('glob')

function getEntry(globPath) {
  let entries = {},
      basename, tmp, pathname, appname;

  glob.sync(globPath).forEach(function(entry) {
      basename = path.basename(entry, path.extname(entry));
      tmp = entry.split('/').splice(-3);
      pathname = basename; // 正确输出js和html的路径
      // console.log(pathname)
      entries[pathname] = {
          entry:'src/'+tmp[0]+'/'+tmp[1]+'/'+tmp[1]+'.js',
          template:'src/'+tmp[0]+'/'+tmp[1]+'/'+tmp[2],
          filename:tmp[2]
      };
  });
  return entries;
}

let resolve = dir => path.resolve(__dirname, dir)
let htmls = getEntry('./src/pages/**/*.html');
console.log(htmls);
module.exports = {
  productionSourceMap: false,
  pages: htmls,
  chainWebpack: config => {
    config.module
      .rule('images')
      .use('url-loader')
      .loader('url-loader')
      .tap(options => {
          // 修改它的选项...
          options.limit = 10000
          return options
      })
    config.resolve.alias
      .set('@', resolve('src'))
      .set('components', resolve('src/components'))
      .set('assets', 'src/assets')
      // .set('styles', 'src/styles')
  },
  devServer: {
    port: 8082, // 端口
    open: false, // 启动后打开浏览器
    overlay: {
      //  当出现编译器错误或警告时，在浏览器中显示全屏覆盖层
      warnings: false,
      errors: true
    },
    proxy: {
      //配置跨域
      '/api': {
          target: "https://api.antfact.com/intelligent/analysis",
          ws:true,
          changOrigin:true,
          pathRewrite:{
            '^/api':'/'
          }
      }
    }
  },
  // configureWebpack: {
  //   // 把原本需要写在webpack.config.js中的配置代码 写在这里 会自动合并

  // },
  // chainWebpack: config => {
  //   config.module
  //     .rule('images')
  //     .use('url-loader')
  //     .loader('url-loader')
  //     .tap(options => {
  //         // 修改它的选项...
  //         options.limit = 10000
  //         return options
  //     })
  //   config.resolve.alias
  //     .set('@', resolve('src'))
  //     .set('_c', resolve('src/components'))
  //     .set('_s', resolve('src/style'))
  // }
}

